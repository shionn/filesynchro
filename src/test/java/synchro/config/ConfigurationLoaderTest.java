package synchro.config;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

public class ConfigurationLoaderTest {

    private final ConfigurationLoader loader = new ConfigurationLoader();

    @Test
    public void testLoadFromHome() throws Exception {
        System.setProperty("user.home", "src/test/resources");
        assertThat(loader.loadFromHome()).as("Configuration loaded from home").isNotNull();
    }

}
