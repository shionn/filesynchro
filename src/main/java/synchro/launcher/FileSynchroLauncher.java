/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro.launcher;

import java.awt.AWTException;
import java.io.IOException;

import synchro.Synchro;
import synchro.config.Configuration;
import synchro.config.ConfigurationLoader;
import synchro.config.ConfigurationLoadingException;
import synchro.frame.FileSynchroFrame;
import synchro.frame.SystemTrayControleur;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class FileSynchroLauncher {

    public static void main(String[] args) throws ConfigurationLoadingException, AWTException,
            IOException {
        Configuration configuration = new ConfigurationLoader().load();
        Synchro synchro = initSynchro(configuration);
        FileSynchroFrame frame = initFrame(synchro);
        initSystemTray(frame, synchro);
    }

    private static Synchro initSynchro(Configuration configuration) {
        Synchro synchro = new Synchro();
        synchro.setConfiguration(configuration);
        synchro.init();
        return synchro;
    }

    private static FileSynchroFrame initFrame(Synchro synchro) {
        FileSynchroFrame frame = new FileSynchroFrame();
        frame.setSynchro(synchro);
        frame.init();
        return frame;
    }

    private static void initSystemTray(FileSynchroFrame frame, Synchro synchro)
            throws AWTException, IOException {
        SystemTrayControleur tray = new SystemTrayControleur();
        tray.setFrame(frame);
        tray.setSynchro(synchro);
        tray.init();
    }

}
