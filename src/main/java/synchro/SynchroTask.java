/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro;

import java.io.File;

import synchro.event.StateChangeEvent;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class SynchroTask implements Runnable {

    private SynchroState state = SynchroState.STOP;
    private final File source;
    private final Synchro synchro;
    private final File destination;

    public SynchroTask(Synchro synchro, File source, File destination) {
        this.synchro = synchro;
        this.source = source;
        this.destination = destination;
    }

    public SynchroState getState() {
        return state;
    }

    public void start() {
        changeState(SynchroState.STARTING);
    }

    public void stop() {
        if (state == SynchroState.RUNNING) {
            changeState(SynchroState.STOPING);
        }
    }

    @Override
    public void run() {
        switch (state) {
        case STARTING:
            deleteDestination();
            upload();
            changeState(SynchroState.RUNNING);
            break;
        case RUNNING:
            download();
            changeState(SynchroState.RUNNING);
            break;
        case STOPING:
            download();
            deleteDestination();
            changeState(SynchroState.STOP);
            break;
        }
    }

    private void deleteDestination() {
        String cmd = "rm -Rf " + buildDestination();
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
        } catch (Exception e) {
            synchro.notify(e);
        }
    }

    private void changeState(SynchroState s) {
        state = s;
        synchro.notify(new StateChangeEvent(source, state));
    }

    private void download() {
        changeState(SynchroState.DOWNLOADING);
        String cmd = "rsync -a --delete " + buildDestination() + " " + buildSource();
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
        } catch (Exception e) {
            synchro.notify(e);
        }
    }

    private void upload() {
        changeState(SynchroState.UPLOADING);
        String cmd = "rsync -a " + buildSource() + " " + buildDestination();
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
        } catch (Exception e) {
            synchro.notify(e);
        }
    }

    private String buildSource() {
        return source.getAbsolutePath() + "/";
    }

    private String buildDestination() {
        return destination + "/" + source.getName() + "/";
    }
}
