/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro.frame;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import synchro.Synchro;
import synchro.event.StateChangeEvent;
import synchro.event.SynchroListener;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class FileSynchroFrame extends JFrame implements SynchroListener {
    private static final long serialVersionUID = 5850397885092830019L;
    private Synchro synchro;
    private final Map<String, JButton> buttons = new HashMap<String, JButton>();
    private Icon stop, start, up, down;
    private JLabel info;

    public void init() {
        getContentPane().setLayout(new BorderLayout());
        setTitle("File Synchro by Shionn (shionn.org)");
        setSize(800, 650);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        buildImg();
        buildMainPanel();
        buildInfo();
        synchro.addListener(this);
    }

    private void buildInfo() {
        info = new JLabel();
        updateInfo();
        getContentPane().add(info, BorderLayout.SOUTH);
    }

    private void buildMainPanel() {
        JPanel panel = new JPanel(new GridLayout(0, 2, 0, 0));
        for (File source : synchro.getSources()) {
            JButton button = buildButton(source);
            panel.add(button);
        }
        getContentPane().add(panel, BorderLayout.CENTER);
    }

    private void buildImg() {
        start = new ImageIcon(FileSynchroFrame.class.getClassLoader().getResource("start-32.png"));
        stop = new ImageIcon(FileSynchroFrame.class.getClassLoader().getResource("stop-32.png"));
        up = new ImageIcon(FileSynchroFrame.class.getClassLoader().getResource("up-32.png"));
        down = new ImageIcon(FileSynchroFrame.class.getClassLoader().getResource("down-32.png"));
    }

    private JButton buildButton(final File source) {
        JButton button = new JButton(source.getName());
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (synchro.getState(source)) {
                case STOP:
                    synchro.start(source);
                    break;
                case RUNNING:
                    synchro.stop(source);
                    break;
                }
            }
        });
        buttons.put(source.getName(), button);
        return button;
    }

    public void setSynchro(Synchro synchro) {
        this.synchro = synchro;
    }

    @Override
    public void stateChange(StateChangeEvent event) {
        JButton button = buttons.get(event.getSource().getName());
        switch (event.getState()) {
        case STARTING:
            updateButton(button, start, false);
            break;
        case RUNNING:
            updateButton(button, start, true);
            break;
        case DOWNLOADING:
            updateButton(button, down, false);
            break;
        case UPLOADING:
            updateButton(button, up, false);
            break;
        case STOPING:
            updateButton(button, stop, false);
            break;
        case STOP:
            updateButton(button, stop, true);
            break;
        }
        updateInfo();
    }

    private void updateInfo() {
        long used = synchro.getUsedSpace() / 1024 / 1024;
        long free = synchro.getFreeSpace() / 1024 / 1024;
        long total = synchro.getTotalSpace() / 1024 / 1024;
        info.setText("Used : " + used + " Mo | Free : " + free + " Mo | Total : " + total + " Mo");
    }

    private void updateButton(JButton button, Icon icon, boolean enable) {
        button.setEnabled(enable);
        button.setIcon(icon);
        button.repaint();
    }

    @Override
    public void error(Exception e) {
        StringWriter message = new StringWriter();
        e.printStackTrace(new PrintWriter(message));
        JOptionPane.showMessageDialog(this, e.getMessage() + "\n" + message.toString(), "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void message(String message) {
        JOptionPane.showMessageDialog(this, message.toString(), "Error", JOptionPane.ERROR_MESSAGE);
    }

}
