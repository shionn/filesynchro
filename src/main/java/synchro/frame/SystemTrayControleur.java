/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro.frame;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import synchro.Synchro;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class SystemTrayControleur {

    private JFrame frame;
    private Synchro synchro;

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public void setSynchro(Synchro synchro) {
        this.synchro = synchro;
    }

    public void init() throws AWTException, IOException {
        if (SystemTray.isSupported()) {
            build();
        }
    }

    private void build() throws AWTException, IOException {
        SystemTray tray = SystemTray.getSystemTray();
        tray.add(buildTrayIcon());
    }

    private TrayIcon buildTrayIcon() throws IOException {
        Image image = ImageIO.read(SystemTrayControleur.class.getClassLoader().getResourceAsStream(
                "icon.png"));
        TrayIcon trayIcon = new TrayIcon(image, "FileSynchro", buildPopupMenu());
        trayIcon.setImageAutoSize(true);
        return trayIcon;
    }

    private PopupMenu buildPopupMenu() {
        PopupMenu menu = new PopupMenu();
        menu.add(buildOpenMenuItem());
        menu.add(buildQuitMenuItem());
        return menu;
    }

    private MenuItem buildQuitMenuItem() {
        MenuItem item = new MenuItem("Quit");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                synchro.exit();
            }
        });
        return item;
    }

    private MenuItem buildOpenMenuItem() {
        MenuItem item = new MenuItem("Show / Hide");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(!frame.isVisible());
            }
        });
        return item;
    }

}
