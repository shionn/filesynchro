/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro.config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class ConfigurationLoader {

    public Configuration load() throws ConfigurationLoadingException {
        try {
            Properties props = loadFromHome();
            Configuration configuration = new Configuration();
            configuration.setProperties(props);
            return configuration;
        } catch (IOException e) {
            throw new ConfigurationLoadingException(e);
        }
    }

    Properties loadFromHome() throws IOException {
        String home = System.getProperty("user.home");
        String fileName = home + "/.filesynchro/config.properties";
        Properties props = new Properties();
        props.load(new FileReader(fileName));
        return props;
    }

}
