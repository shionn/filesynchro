/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro.config;

import java.util.Properties;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Configuration {

    private Properties props;

    public void setProperties(Properties props) {
        this.props = props;
    }

    public String getSourceFolder() {
        return props.getProperty("source.folder");
    }

    public String getIgnored() {
        return props.getProperty("source.ignored");
    }

    public int getThreadPoolSize() {
        return Integer.parseInt(props.getProperty("thread.pool.size", "1"));
    }

    public String getDestinationFolder() {
        return props.getProperty("dest.folder");
    }

    public int getThreadLoopTime() {
        return Integer.parseInt(props.getProperty("thread.loop.time", "1"));
    }

    public int getToolCount() {
        return Integer.parseInt(props.getProperty("source.tool.count", "0"));
    }

    public String getToolSource(int i) {
        return props.getProperty("source.tool." + i);
    }

}
