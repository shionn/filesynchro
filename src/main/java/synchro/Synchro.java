/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 *
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 * GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+ G- e+++ h+ r- !y-
 */
package synchro;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import synchro.config.Configuration;
import synchro.event.StateChangeEvent;
import synchro.event.SynchroListener;

/**
 * Code sous licence GPLv3 (http://www.gnu.org/licenses/gpl.html)
 * 
 * @author <b>Shionn</b>, shionn@gmail.com <i>http://shionn.org</i><br>
 *         GCS d- s+:+ a- C++ UL/M P L+ E--- W++ N K- w-- M+ t+ 5 X R+ !tv b+ D+
 *         G- e+++ h+ r- !y-
 */
public class Synchro {
    private Configuration configuration;
    private List<File> sources;
    private final List<SynchroListener> listeners = new ArrayList<SynchroListener>();
    private final HashMap<File, SynchroTask> tasks = new HashMap<File, SynchroTask>();
    private ScheduledExecutorService executor;
    private File destination;

    public void init() {
        File source = new File(configuration.getSourceFolder());
        destination = new File(configuration.getDestinationFolder());
        sources = new ArrayList<File>();
        for (int i = 0; i < configuration.getToolCount(); i++) {
            sources.add(new File(configuration.getToolSource(i)));
        }
        sources.addAll(listeSources(source));
        executor = Executors.newScheduledThreadPool(configuration.getThreadPoolSize());
    }

    private List<File> listeSources(File source) {
        List<File> sources = Arrays.asList(source.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !name.matches(configuration.getIgnored());
            }
        }));
        Collections.sort(sources, new Comparator<File>() {
            @Override
            public int compare(File f1, File f2) {
                return f1.getName().toLowerCase().compareTo(f2.getName().toLowerCase());
            }
        });
        return sources;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public List<File> getSources() {
        return sources;
    }

    public void addListener(SynchroListener listener) {
        listeners.add(listener);
    }

    public SynchroState getState(File source) {
        SynchroTask task = tasks.get(source);
        if (task == null) {
            return SynchroState.STOP;
        }
        return task.getState();
    }

    public void start(File source) {
        SynchroTask task = tasks.get(source);
        if (task == null) {
            task = buildTask(source);
        }
        task.start();
    }

    private SynchroTask buildTask(File source) {
        SynchroTask task = new SynchroTask(this, source, destination);
        executor.scheduleAtFixedRate(task, 1, configuration.getThreadLoopTime() * 60,
                TimeUnit.SECONDS);
        tasks.put(source, task);
        return task;
    }

    public void stop(File source) {
        SynchroTask task = tasks.get(source);
        if (task != null) {
            task.stop();
        }
    }

    public void notify(StateChangeEvent event) {
        for (SynchroListener listener : listeners) {
            listener.stateChange(event);
        }
    }

    public void notify(Exception e) {
        for (SynchroListener listener : listeners) {
            listener.error(e);
        }
    }

    public void notify(String message) {
        for (SynchroListener listener : listeners) {
            listener.message(message);
        }
    }

    public void exit() {
        boolean canExit = true;
        Iterator<SynchroTask> ite = tasks.values().iterator();
        while (canExit && ite.hasNext()) {
            SynchroTask task = ite.next();
            canExit = task.getState() == SynchroState.STOP;
        }
        if (canExit) {
            System.exit(0);
        } else {
            notify("All Process should be stopped !");
        }
    }

    public long getUsedSpace() {
        return destination.getTotalSpace() - destination.getFreeSpace();
    }

    public long getFreeSpace() {
        return destination.getFreeSpace();
    }

    public long getTotalSpace() {
        return destination.getTotalSpace();
    }
}
